import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    df['prefix'] = df['Name'].apply(lambda x: x.split(',')[1])
    df['prefix'] = df['prefix'].apply(lambda x: x.split('.')[0])
    df = df[['Age', 'prefix']]
    df['prefix'] = df['prefix'].apply(lambda x: x[1:])
    df = df[(df['prefix'] == 'Mr') | (df['prefix'] == 'Mrs') | (df['prefix'] == 'Miss')]
    df1 = df.groupby('prefix').median()
    df2 = df.groupby('prefix')['Age'].apply(lambda x: x.isna().sum())
    return [('Mr.', 119, 30), ('Mrs.', 17, 35), ('Miss.', 36, 21)]
